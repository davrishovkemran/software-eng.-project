# Gereksinim Analizi(Projenin Amacı) #

   Türlü sosyal medya araçları ve farklı ortak paylaşım platformlarında insanlar 
birbirleriyle yazı, fikir ve görüş gibi çeşitli paylaşımlar yapmaktan mutluluk duyuyorlar. Bizim projenin amacı, herkesin kendine özgü fikirleriyle ortak hikayeler yazabilmelerini sağlayan web arayüzlü bir platform (web sitesi) geliştirmektir.

* Site için kullanıcı kayıt sayfası.
* Kayıtlı kullanıcıların giriş yapabıleceği login sayfası
* Yeni Hikayeler oluşturabileceğin bir sayfa.
* Güncel olarak katkı yapılan hikayelerin sıralandığı Ana sayfa.
  Burda farklışekillerde filtreleme ve arama yapılabilir.

### Fonksiyonel Olmayan Gereksinimler ###

* Site için DNS alınması
* Sistemimizin gerekliliklerine uygun olan Server'ların alınması
* Server'larda çalışacak gerekli yazılımların temin edilmesi
### ... ###
* [Detay için tıkla...](https://bitbucket.org/davrishovkemran/software-eng.-project/src/418b16100080e997c185faf752422039392ab756?at=master)

