

from django.conf.urls import url, include
from django.contrib import admin
from storyboard.urls import urlpatterns as story_urls
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^accounts/login/$', auth_views.LoginView.as_view(), name="login"),
    url(r'^accounts/logout/$', auth_views.LogoutView.as_view(next_page="/"), name="logout"),
    url(r"^", include(story_urls)),
]
